package route

import (
	"calculator/api"

	"github.com/gofiber/fiber/v2"
)

func Route(app *fiber.App) {
	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	app.Get("/api/calc", api.Test)
}
