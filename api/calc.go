package api

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
)

func Test(c *fiber.Ctx) error {
	request := struct {
		Op    string `binding: "required"`
		Left  int    `binding: "required"`
		Right int    `binding: "required"`
	}{}

	if err := c.BodyParser(&request); err != nil {
		return err
	}

	switch request.Op {
	case "+":
		total := request.Left + request.Right

		return c.JSON(&fiber.Map{
			"code":    200,
			"result":  total,
			"message": fmt.Sprintf("Penjumlahan berhasil"),
		})
	case "-":
		total := request.Left - request.Right

		return c.JSON(&fiber.Map{
			"code":    200,
			"result":  total,
			"message": fmt.Sprintf("Pengurangan berhasil"),
		})
	case "*":
		total := request.Left * request.Right

		return c.JSON(&fiber.Map{
			"code":    200,
			"result":  total,
			"message": fmt.Sprintf("Perkalian berhasil"),
		})
	case "/":
		if request.Right == 0.0 {
			total := "~"

			return c.JSON(&fiber.Map{
				"code":    201,
				"result":  total,
				"message": fmt.Sprintf("Pembagian gagal, hasil tak terhingga"),
			})
		} else {
			total := request.Left / request.Right

			return c.JSON(&fiber.Map{
				"code":    200,
				"result":  total,
				"message": fmt.Sprintf("Pembagian berhasil"),
			})
		}
	default:
		return c.JSON(&fiber.Map{
			"code":    201,
			"result":  fmt.Sprintf("No result"),
			"message": fmt.Sprintf("Operand not found"),
		})
	}
}
