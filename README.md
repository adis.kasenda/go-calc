# go-calc



## Getting started how to setup

1. Clone project with command `git clone https://gitlab.com/adis.kasenda/go-calc.git`
2. Run `go get`
3. Please fill the body with :

{
    "Op" : "/",
    "Left" : int,
    "Right" : int
}

4. "Op" can fill the operand like / * + -
5. Run `go run main.go`
6. Open in your browser with `localhost:3000/api/calc` 