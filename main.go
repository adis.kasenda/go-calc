package main

import (
	"calculator/route"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()

	route.Route(app)

	app.Listen(":3000")
}
